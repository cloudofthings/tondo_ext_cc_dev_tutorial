import argparse
import requests
from threading import Thread

parser = argparse.ArgumentParser(description='Send a datapoint to DeviceTone cloud')
parser.add_argument('-deviceid', help='DeviceTone device identifier',required=True)
parser.add_argument('-streamid', help='DeviceTone stream identifier',required=True)
parser.add_argument('-apikey', help='a valid API key',required=True)
parser.add_argument('-content', help='the content to send',required=True)


args = parser.parse_args()


def send_datapoint():
	FQDN = "api.cloudofthings.com"

	PATH = "https://"+FQDN+"/v3/devices/"+args.deviceid+"/streams/"+args.streamid+"/"

	headers = {'X-API':args.apikey}
	r = requests.put(PATH,data = {'content':args.content},headers = headers)
	print("HTTP CODE: "+str(r.status_code))
	print("CONTENT: "+r.text)
	
Thread(target=send_datapoint).start()

