import argparse
import requests
from threading import Thread

parser = argparse.ArgumentParser(description='Get device list')
parser.add_argument('-username', help='DeviceTone project user',required=True)
parser.add_argument('-password', help='DeviceTone project user password',required=True)
parser.add_argument('-companyid', help='DeviceTone company id',required=True)


args = parser.parse_args()


def get_devicelist():
	FQDN = "api.cloudofthings.com"
	PATH = "https://"+FQDN+"/v3/companies/"+args.companyid+"/users/login/"
	r = requests.post(PATH,data = {"password":args.password,"username":args.username})
	apikey = r.json().get('apikey')
	
	PATH = "https://"+FQDN+"/v3/devices/"
	headers = {'X-API':apikey}
	r = requests.get(PATH,headers = headers)
	print("HTTP CODE: "+str(r.status_code))
	print("CONTENT: "+r.text)
	
Thread(target=get_devicelist).start()

