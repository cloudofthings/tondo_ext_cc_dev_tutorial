# Tondo C&C integration examples #

This folder includes examples of the operations and the processes to do basic stuff.

For a complete list of APIs and configurations relevant to C&C, refer to the [POSTMAN files](../postman).

Examples are written in Python.

### Example #1 - Sending data ###

* This is an example Python script that gets the deviceid, API key, streamid and value and posts it to the cloud.

```
python dtsenddata.py
usage: dtsenddata.py [-h] -deviceid DEVICEID -streamid STREAMID -apikey APIKEY -content CONTENT
```


### Example #2 - Querying the report API ###

* This is an example Python script that gets the credentials from a user, and queries the first project associated with this user for information.

```
python dtreportapi.py
usage: dtreportapi.py [-h] -username USERNAME -password PASSWORD -companyid COMPANYID
```                      

### Example #3 - Getting the current list of devices in a project ###

* This is an example Python script that reads the list of devices in the first project associated with a user using the DeviceTone API.

```
python dtgetdevicelist.py
usage: dtgetdevicelist.py [-h] -username USERNAME -password PASSWORD -companyid COMPANYID
```


