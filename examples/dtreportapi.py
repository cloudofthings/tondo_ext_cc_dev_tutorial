import argparse
import requests
from threading import Thread

parser = argparse.ArgumentParser(description='Query the report API')
parser.add_argument('-username', help='DeviceTone project user',required=True)
parser.add_argument('-password', help='DeviceTone project user password',required=True)
parser.add_argument('-companyid', help='DeviceTone company id',required=True)


args = parser.parse_args()


def get_devicelist():
	FQDN = "api.cloudofthings.com"
	PATH = "https://"+FQDN+"/v3/companies/"+args.companyid+"/users/login/"
	r = requests.post(PATH,data = {"password":args.password,"username":args.username})
	apikey = r.json().get('apikey')
	
	PATH = "https://"+FQDN+"/v3/projects/"
	headers = {'X-API':apikey}
	r = requests.get(PATH,headers = headers)
	results = r.json()
	
	if (results.get('count')>=1):
		projectid = results.get('results')[0].get('id')
		print(projectid)
		PATH = "https://"+FQDN+"/v3/projects/"+projectid+"/reporting_query/"
		headers = {'X-API':apikey}
		r = requests.post(PATH,json = {"where":[],"columns":["sys___online","meta.name","id","power"],"order_by":"id asc","page":0},headers = headers)
		print("HTTP CODE: "+str(r.status_code))
		print("CONTENT: "+r.text)
	else:
		print("No project for this user")
	
Thread(target=get_devicelist).start()

