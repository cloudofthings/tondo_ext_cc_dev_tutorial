# Architecture

The Tondo backend is based on the Cloud of Things DeviceTone backend.

The DeviceTone backend is a fully-blown device management infrastructure for IoT systems and includes all the functions needed from a production-grade IoT system.

For more information about the system architecture, please refer to the [DeviceTone documentation](https://docs.cloudofthings.com/index.html).


### Connectivity principles ###

* Performing actions is usually done using REST APIs commands vs. the API v3 backend of the DeviceTone.
* Project-based access is usually done using a project user/password and a company id. When used in conjunction with the Login API the system returns a token for access ([API Key](https://docs.cloudofthings.com/developers-guides/keys.html)). A user can have project-level access to more than one project.
* To get the list of available projects for a user, use the Project API to get the list of projects for this account.
* The system supports getting device-level notifications using two methods - MQTT or Triggers set in the backend.
* For most C&C systems that need aggergative/tabular functionality, it is recommended to use the "Report API" that provides an online, live picture of the current status for all devices in the project.


## Scenarios ##

* For actual code that shows how this works in code, please refer to the [examples](examples) section.

* To get started working, you can use the [postman](postman) files under the postman folder.


#### Scenario #1 - Getting the status of the devices in the project ####

* Call the RESTful "Login API" - use the project user / project password and company id you have to get a valid API key needed for the next steps.
* Query the RESTful "Report API". You can pass filters to the API in the POST header to get a subgroup of the devices.

#### Scenario #2 - Subcribing to statuses of single devices ####

* Call the RESTful "Login API" - use the project user / project password and company id you have to get a valid API key needed for the next steps.
* Use your deviceid as username and the API key as password to subscribe to the device's [MQTT streams](https://docs.cloudofthings.com/developers-guides/MQTT_api.html).

#### Scenario #3 - Turning on a lamp ####

* Call the RESTful "Login API" - use the project user / project password and company id you have to get a valid API key needed for the next steps.
* Use the RESTful GET "Device Streams API" to get the list of data streams of your device.
* Call the RESTful PUT "Device Streams API" to write the new value to the "power" stream of your device.

#### Scenario #4 - Getting the current list of devices in a project ####

* Call the RESTful "Login API" - use the project user / project password you have to get a valid API key needed for the next steps.
* Call the RESTful GET "Device API" - use the key to get the list of devices you have access to. You can filter by project id.



