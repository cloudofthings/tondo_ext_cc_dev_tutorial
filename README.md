# Tondo C&C integration #

This repo holds instructions and examples for connecting to the Tondo Smart Lighting backend.

### Summary of takeaways ###

* Understand the general [architecture, connectivity principles and API instructions](Architecture.md)
* [Principal actions](examples/Examples.md) supported
* Other resources - POSTMAN api summary and env files are available under the [postman](postman) directory

### Prerequisites ###

* A username/password to the backend platform (provided to you by the Tondo team)
* A project should be set-up in the system
* The company id in the system
* A dev environment that can communicate using [REST](https://docs.cloudofthings.com/developers-guides/HTTP-access.html) and [MQTT](https://docs.cloudofthings.com/developers-guides/MQTT_api.html) (such as Python, .NET, Node.js, etc.)

### Need support? ###

* Please contact your Tondo contact for more information
